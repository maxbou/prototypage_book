# Programme de la journée

## Matin

* Présentation  de la Winter School \[10h - 10h30\]

* Impression 3D \[10h30- 13h\]

  * "Talk"

    * Impression 3D: Kezako

    * Principes généraux

    * Environnement logiciel / Hardware

  * Workshop

    * Modelisation 3D dans Google Sketchup

    * Slicing dans Cura

    * Début des impressions \(Ultimaker 2\)

## Aprem

* Présentation du prototypage elec \[14h - 14h30\]

  * Mouvement maker, prototypage rapide, revolution Arduino / Raspberry

  * Raspberry vs Arduino: Dawn Of Justice

* Arduino \[14h30 - 20H\]

  * Talk

    * SoftwareSoftware

    * Hardware

    * Family

  * Workshop

    * Faire blink une LED

    * Allumer une LED avec un bouton

    * Allumer un LED avec un bouton et la contrôler avec un potentiometre

# Contenu

## Impression 3D: Kezako

* Vieux principe, démocratisé recement, pierre angulaire de la révolution maker
* Des énormes avantages, mais des gros défauts également :
  * * Chaîne de production rapide /  - Production lente
  * * Differentes matières avec des propriétés différentes / - Un nombre limité de matières
  * * Des pièces impossible à créer par l'industrie classique / - Une finition à faire, et des contraintes à maîtriser![](/assets/import.png)
  * 



