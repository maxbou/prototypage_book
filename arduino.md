## ![](/assets/logo.png)

## 

## Learning

Getting started : [https://www.arduino.cc/en/Guide/HomePage](https://www.arduino.cc/en/Guide/HomePage)

Tutorials : [https://www.arduino.cc/en/Tutorial/HomePage](https://www.arduino.cc/en/Tutorial/HomePage)

Reference : [https://www.arduino.cc/en/Reference/HomePage](https://www.arduino.cc/en/Reference/HomePage)

Libs : [https://www.arduino.cc/en/Reference/Libraries](https://www.arduino.cc/en/Reference/Libraries)

![](http://boutique.semageek.com/2-1338-thickbox_default/arduino-uno-dip-rev3.jpg)

[https://en.wikipedia.org/wiki/Arduino](https://en.wikipedia.org/wiki/Arduino)

![](https://upload.wikimedia.org/wikipedia/commons/thumb/0/0c/ATMEGA328P-PU.jpg/250px-ATMEGA328P-PU.jpg)

[https://en.wikipedia.org/wiki/ATmega328](https://en.wikipedia.org/wiki/ATmega328)

