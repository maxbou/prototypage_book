## Impression 3D: Kezako

* Vieux principe, démocratisé recement, pierre angulaire de la révolution maker
* Des énormes avantages, mais des gros défauts également :

  * * Chaîne de production rapide /  - Production lente
  * * Differentes matières avec des propriétés différentes / - Un nombre limité de matières
  * * Des pièces impossible à créer par l'industrie classique / - Une finition à faire, et des contraintes à maîtriser![](/assets/import.png)
    * Peut échouer si l'on utilise les mauvais paramètres ou qu'on fait n'importe quoi
    * ![](/assets/import2.png)

* Il existe plusieurs types d'imprimantes 3D:

  * [Bain liquide + laser / lampe](https://www.youtube.com/watch?v=yYGycgnYlBM)
  * [Extrusion de plastique ](https://www.youtube.com/watch?v=WHO6G67GJbM)
  * [Poudre + laser ](https://www.youtube.com/watch?v=wD9-QEo-qDk)
  * [Et autres trucs de Hippies](https://www.youtube.com/watch?v=6C7bjzIW610)



