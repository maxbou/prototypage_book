# Winter is coming

Ce GITBOOK est a destination des étudiants résidents du Hub Innovation.  
Si vous n'êtes pas résident, arretez de lire ceci et serieux, bougez vous un peu quoi!

Ce book servira de support à la \(petite\) semaine de formation que vous suivrez, du 16 au 18 Janvier 2017 \(première édition\). N'hésitez pas à l'utiliser pour votre formation ainsi que pour la formation que vous donnerez aux étudiants lors de la Winter Week.

Ce Book est le VOTRE, vous êtes invités à le lire, mais aussi à l'agrémenter et à l'améliorer.

N'hésitez pas à y rajouter les eventuelles notes que vous prendriez lors de la formation, à rajouter des screenshots, à rajouter vos fichiers sur le git, ou à corriger les fautes / erreurs \(cligne cligne les astextes\), etc.

