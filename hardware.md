# Symboles

[https://en.wikipedia.org/wiki/Electronic\_symbol](https://en.wikipedia.org/wiki/Electronic_symbol)



# Distributeurs

## Adafruit

[https://www.adafruit.com/](https://www.adafruit.com/)

[https://learn.adafruit.com/](https://learn.adafruit.com/)

## Sparkfun

[https://www.sparkfun.com/](https://www.sparkfun.com/)

[https://learn.sparkfun.com/tutorials](https://learn.sparkfun.com/tutorials)

## McHobby

[https://shop.mchobby.be/](https://shop.mchobby.be/)

[https://wiki.mchobby.be/index.php?title=Accueil](https://wiki.mchobby.be/index.php?title=Accueil)

# Tools

[http://www.pighixxx.com/test/tools/](http://www.pighixxx.com/test/tools/)

