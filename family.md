# Arduino family

[https://www.arduino.cc/en/Main/Products](https://www.arduino.cc/en/Main/Products)

[http://www.pighixxx.com/test/pinoutspg/](http://www.pighixxx.com/test/pinoutspg/)

---

## Arduino Uno

![](http://www.semageek.com/wp-content/uploads/2011/12/arduino-uno-est-passe-en-revision-r3-details-des-differences-02.jpg)

![](https://camo.githubusercontent.com/c8729115db0d32684e57eb5728ac9e604e985df8/687474703a2f2f746f7765626f66666963652e636f6d2f776f2f77702d636f6e74656e742f75706c6f6164732f323031322f30372f61726475696e6f5f756e6f5f322e6a7067)

## ![](http://www.pighixxx.com/test/wp-content/uploads/2014/11/uno.png)

---

## Arduino Nano

![](https://wiki.eprolabs.com/images/d/d6/Ard_nano.jpg)

![](https://wiki.eprolabs.com/images/6/67/Nano.jpg)

![](http://www.pighixxx.com/test/wp-content/uploads/2014/11/nano.png)

## Feather 32u4 BLE

![](https://shop.mchobby.be/3290-thickbox_default/feather-32u4-avec-bluefruit-ble.jpg)

![](https://shop.mchobby.be/3291-thickbox_default/feather-32u4-avec-bluefruit-ble.jpg)

![](http://www.pighixxx.com/test/wp-content/uploads/2016/02/2829_pinout_v1_0.png)

