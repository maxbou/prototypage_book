# IDE

[https://www.arduino.cc/en/main/software](https://www.arduino.cc/en/main/software)

![](http://www.hobbytronics.co.uk/image/data/tutorial/arduino-install/install_6.jpg)

# Simulation

[https://circuits.io/home/explore](https://circuits.io/home/explore)

[https://circuits.io/circuits/1767871-control-de-cinco5-servos](https://circuits.io/circuits/1767871-control-de-cinco5-servos)

![](https://i1.wp.com/blog.123dapp.com/files/123DCircuits_Moneyshot_WhiteBG.jpeg)

