L'impression 3D se passe généralement en deux phases, la conception et l'impression.  
Cependant, il est conseillé de savoir l'imprimpante que l'on va utiliser, puisque la technologie de celle-ci a des contraintes particulières qu'il va faloir include dans la conception.

Par exemple, une impression avec un extrudeur necessite d'utiliser des supports pour supporter les parties "dans le cide" d'une pièce, ce qui peut être un problème pour certaines pièces.



Nous allons passer rapidement sur les deux aspects de l'impression, via les logiciels Sketchup \(conception\) et Cura \(qui va générer le fichier pour l'imprimante 3D\). Il s'agit de deux logiciels simples, la conception et l'impression pouvant être bien entendu plus poussés et maitrisés à l'aide d'autres logiciels tels que Inventor, Maya, AutoCAD, \(...\) pour la conception et Slic3r \(...\) pour l'impression. N'hesitez pas à vous renseigner ou me demander pour plus de détails, mais en 1/2 journée c'est short.





