# Summary

* [Introduction](README.md)
* [Day 1](day-1.md)
  * [Impression3D](impression3d.md)
    * [Principes generaux](principes-generaux.md)
    * [Environnement software \/ hardware](environnement-software--hardware.md)
  * [Arduino](arduino.md)
    * [Software](software.md)
    * [Family](family.md)
    * [Hardware](hardware.md)
* [Day 2](day-2.md)
* [Day 3](day-3.md)



