# Programme de la journée

## Matin

* Présentation de l'informatique embarquée \[10h - 10h30\]

* Raspberry PI \[10h30 - 13h\]

  * Talk

    * Raspberry Pi, Pi2, Pi3, Zero: ~~WTF~~ Kezako

    * Principes généraux

    * Environnement logiciel / Hardware

  * Workshop

## Après midi

* Raspberry PI \[14h - 20h\]

* * Workshop



